let carInventory = require('./carData.cjs');

const carYears = [];

for(let index = 0; index < carInventory.length; index++){
    carYears[index] = carInventory[index].car_year;
}

module.exports = carYears;
