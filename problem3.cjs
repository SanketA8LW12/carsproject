// https://www.scaler.com/topics/javascript-sort-an-array-of-objects/  article link to sort by name

let carInventory = require('./carData.cjs');

carInventory.sort(function(a, b) {
    const carNameA = a.car_model.toUpperCase();
    const carNameB = b.car_model.toUpperCase();
    if(carNameA <carNameB){
        return -1;
    }
    if(carNameA > carNameB){
        return 1;
    }
    return 0; // if both the names are same
});

module.exports = carInventory;

  
