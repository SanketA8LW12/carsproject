let carYearList = require('./problem4.cjs');

let carBefore2000 = [];

for(let index = 0; index < carYearList.length; index++){
    if(carYearList[index] < 2000){
        carBefore2000.push(carYearList[index]);
    }
}

module.exports = carBefore2000;
